const crypto = require('crypto')
const fs = require('fs')
const YAML = require('js-yaml')
const path = require('path')
const thunky = require('thunky')
const debug = require('@coboxcoop/logger')('@coboxcoop/repository')
const maybe = require('call-me-maybe')
const { EventEmitter } = require('events')
const { assign, keys, values } = Object
const mutexify = require('mutexify')

const { uniq } = require('./util')

module.exports = (storage, factory, opts) => new Repository(storage, factory, opts)

class NotFoundError extends Error {
  constructor (message) {
    super(message)
    this.name = this.constructor.name
    Error.captureStackTrace(this, this.constructor)
    this.notFound = true
  }
}

class FoundError extends Error {
  constructor (message) {
    super(message)
    this.name = this.constructor.name
    Error.captureStackTrace(this, this.constructor)
    this.found = true
  }
}

class Repository extends EventEmitter {
  constructor (storage, factory, opts = {}) {
    super()
    this._id = crypto.randomBytes(2).toString('hex')
    this.root = storage
    this.inMemory = !storage
    this.storage = !this.inMemory ? path.join(storage, 'entries.yml') : null
    this.writerlock = mutexify()
    this.opts = opts
    this.factory = factory
    this.collection = {}
    this._onCreate = opts.onCreate || skip
    this._readyCallback = thunky(this._ready.bind(this))
    this._closeCallback = thunky(this._close.bind(this))
    this.isReady = null
  }

  ready (callback) {
    return maybe(callback, new Promise((resolve, reject) => {
      this._readyCallback((err) => {
        if (err) return reject(err)
        this.isReady = true
        this._log({ msg: `repository loaded with ${uniq(values(this.collection)).length}` })
        return resolve()
      })
    }))
  }

  close (callback) {
    return maybe(callback, new Promise((resolve, reject) => {
      this._closeCallback((err) => {
        this._log({ err, msg: `closing repository` })
        if (err) return reject(err)
        else resolve()
      })
    }))
  }

  first (callback) {
    return uniq(values(this.collection))[0]
  }

  all (callback) {
    return maybe(callback, new Promise((resolve, reject) => {
      this.ready()
        .catch(reject)
        .then(() => resolve(uniq(values(this.collection))))
    }))
  }

  where (params = {}, callback) {
    return maybe(callback, new Promise((resolve, reject) => {
      this.all()
        .catch(reject)
        .then((entries) => {
          var paramKeys = keys(params)
          if (!paramKeys.length) return reject(new Error('no parameters provided'))

          for (var i = 0; i < paramKeys.length; ++i) {
            var key = paramKeys[i]
            entries = entries.filter(onfilter)

            function onfilter (entry) {
              var attrs = entry.attributes
              if (!attrs[key] || !params[key]) return false
              return attrs[key].toString('hex') === params[key].toString('hex')
            }
          }

          if (!entries.length) return reject(new NotFoundError('entry not found'))
          return resolve(entries)
        })
    }))
  }

  findBy (params = {}, callback) {
    return maybe(callback, new Promise((resolve, reject) => {
      this.where(params).catch(reject).then((entries = []) => {
        resolve(entries[0])
      })
    }))
  }

  create (params = {}, callback) {
    return maybe(callback, new Promise((resolve, reject) => {
      this.findBy(params)
        .then((entry) => reject(new FoundError('entry already exists')))
        .catch((err) => {
          if (!err.notFound) {
            this._log({ err, msg: 'failed to create', params })
            return reject(err)
          }

          var entry = this._build(params)
          entry.ready((err) => {
            if (err) return reject(err)
            this.emit('entry', entry)
            this._cache(entry)
            this.save((err) => {
              if (err) return reject(err)
              return resolve(entry)
            })
          })
        })
    }))
  }

  save (callback) {
    if (this.inMemory) return callback()

    this.writerlock((release) => {
      this.all((err, entries) => {
        if (err) debug(err)
        this._log({ msg: `saving ${entries.length} records` })
        var dump = entries.reduce((acc, entry, i) => {
          acc[entry.repositoryId] = entry.attributes
          return acc
        }, {})

        let saved
        try {
          saved = YAML.safeDump(dump, { sortKeys: true })
        } catch (err) {
          return release(callback, new Error('Failed to save! Fatal Error! Oh no!', err))
        }

        fs.writeFile(this.storage, saved, (err) => {
          if (err) this._log({ err, msg: 'failed to save to storage directory' })
          return release(callback, err)
        })
      })
    })
  }

  load (callback) {
    if (this.inMemory) return callback()

    return maybe(callback, new Promise((resolve, reject) => {
      fs.readFile(this.storage, 'utf8', (err, data) => {
        if (err) {
          this._log({ err, msg: 'failed to load from storage directory' })
          return reject(err)
        }

        var collection = YAML.safeLoad(data)
        this._seed(collection)
        return resolve()
      })
    }))
  }

  remove (params = {}, callback) {
    return maybe(callback, new Promise((resolve, reject) => {
      this.findBy(params)
        .then((entry) => {
          values(entry.attributes).forEach((attr) => delete this.collection[attr])
          this.save((err) => {
            if (err) return reject(err)
            return resolve()
          })
        }).catch((err) => {
          if (err.notFound) {
            reject(new FoundError('entry does not exist'))
          } else if (!err.notFound) {
            this._log({ err, msg: 'failed to remove', params })
            return reject(err)
          }
        })
    }))
  }

  // ------------------------------------------------------------------

  _initialize (callback) {
    if (this.inMemory) return callback()

    this.writerlock((release) => {
      fs.mkdir(this.root, { recursive: true }, (err) => {
        if (err) {
          this._log({ err, msg: 'failed to create root storage directory' })
          return release(callback, err)
        }

        fs.writeFile(this.storage, YAML.safeDump({}, { sortKeys: true }), (err) => {
          if (err) this._log({ err, msg: 'failed to write to storage directory' })
          return release(callback, err)
        })
      })
    })
  }

  _seed (collection = []) {
    values(collection).forEach((params, i) => {
      var entry = this._build(params)
      this._cache(entry)
    })
  }

  _ready (callback) {
    var self = this

    loadOrInit((err) => {
      if (err) return callback(err)

      var pending = 1
      var collection = uniq(values(this.collection))

      function next (n) {
        var entry = collection[n]
        if (!entry) return done()
        ++pending
        process.nextTick(next, n + 1)
        entry.ready((err) => {
          if (err) return done(err)
          self.emit('entry', entry)
          done()
        })
      }

      function done (err) {
        if (err) {
          pending = Infinity
          self._log({ err })
          return callback(err)
        }
        if (!--pending) {
          return callback()
        }
      }

      next(0)
    })

    function loadOrInit (cb) {
      if (self.isReady) return cb()
      fs.exists(self.storage, (bool) => {
        if (!bool) return self._initialize(done)
        return done()

        function done () {
          return self.load(cb)
        }
      })
    }
  }

  _close (callback) {
    var self = this
    this.save((err) => {
      if (err) return callback(err)
      var pending = 1
      var collection = uniq(values(this.collection))

      function next (n) {
        var entry = collection[n]
        if (!entry) return done()
        ++pending
        process.nextTick(next, n + 1)
        entry.close(done)
      }

      function done (err) {
        if (err) {
          pending = Infinity
          self._log({ err })
          return callback(err)
        }
        if (!--pending) {
          return callback()
        }
      }

      next(0)
    })
  }

  _build (params = {}) {
    return this.factory(assign(params))
  }

  _cache (entry) {
    values(entry.attributes).forEach((attr) => this.collection[attr] = entry)
  }

  _log (opts) {
    debug({
      id: this._id,
      location: this.root || 'in-memory',
      ...opts
    })
  }
}

function skip (entry, cb) { return cb() }
function noop () {}
