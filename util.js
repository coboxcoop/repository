function isString (variable) {
  return typeof variable === 'string'
}

function isFunction (variable) {
  return variable && typeof variable === 'function'
}

function uniq (array) {
  if (!Array.isArray(array)) array = [array]
  return Array.from(new Set(array))
}

function removeEmpty (obj) {
  return Object.keys(obj)
    .filter(k => obj[k] != null)
    .reduce((newObj, k) => {
      return typeof obj[k] === "object"
        ? { ...newObj, [k]: removeEmpty(obj[k]) }
        : { ...newObj, [k]: obj[k] }
    }, {})
}

module.exports = {
  isString,
  isFunction,
  removeEmpty,
  uniq
}

