# Repository

<div align="center">
  <img src="https://cobox.cloud/src/svg/logo.svg">
</div>

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

## Table of Contents

  - [About](#about)
  - [Install](#install)
  - [Usage](#usage)
  - [API](#api)
  - [Contributing](#contributing)
  - [License](#license)

## About
**CoBox** is an encrypted p2p file system and distributed back-up tool. [README](https://gitlab.com/coboxcoop/readme) provides a map of the project.

`repository` provides a simple repository pattern for caching, creating, loading and saving the following models:

* [@coboxcoop/space](https://gitlab.com/coboxcoop/space)
* [@coboxcoop/replicator](https://gitlab.com/coboxcoop/replicator)
* [@coboxcoop/superusers](https://gitlab.com/coboxcoop/superusers)

### Why?

In CoBox we're using this to load each multifeed and kappa-core instance and cache them in this repository. This ensures that our indexes are always up to date by calling the ready function on each instance before returning the instance itself. The basic querying functionality allows for our controllers to pass a set of parameters to a `where` or `findBy` function and match on that basis.

Currently supports matching parameters based on the following types:
* Buffers
* Strings

## Install

```
npm i -g @coboxcoop/schemas

```

## Usage

```
const Repository = require('@coboxcoop/repository')

const repository = Repository(storage, factory, opts)
```

All `opts` get passed down to the instance generated by the factory.

Here's an example of what a controller would look like using `@coboxcoop/repository`:

```js
const Repository = require('@coboxcoop/repository')

// Suppose a POST create has been sent with the following parameters
var params = {
  name: 'myspace',
  address: '4cef27e9702ae90c07056aa88cb4527a7037368311c0b0ed58db28344613ebd6'
}

function SpacesController () {
  const spaces = Repository('path/to/yaml/file/for/spaces', createSpace, { onCreate })

  return { create }

  async function create (request, response) {
    try {
      await spaces.ready()
      var space = await spaces.create(request.params)
      response.send('success! you created a space')
    } catch (err) {
      if (err.found) return response.send('validation failed: space with these parameters already exists')
    }
  }

  function onCreate (entry, cb) {
    // callback executes just before caching, but after building
  }
}
```

## API

All functions can be called either with a callback as the last argument, or using async/await (i.e. they return a promise).

### `repository.all()`
Get all entries

### `repository.where(attributes)`
Get all entries that match the strongly matches the attributes provided (will ignore if one or more attributes doesn't match while but others match)

### `repository.findBy(attributes)`
Get first entry that matches attributes provided (same as where but returns the first one)

### `repository.create(attributes)`
Use the factory function provided to build an instance and cache it. `ready` function expected on the entry class produced by the factory.

### `repository.ready()`
Load and get all entries ready. should always be called first.

### `repository.close()`
Gracefully close all entries. `close` function expected on the entry class produced by the factory

### `repository.save()`
Persist all entries to a YAML file. create one if it doesn't exist already.

### `repository.load()`
Load all entries from the YAML file.


## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

# License

[`AGPL-3.0-or-later`](./LICENSE)
