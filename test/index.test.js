const { describe } = require('tape-plus')
const sinon = require('sinon')
const path = require('path')
const proxyquire = require('proxyquire')
const fs = require('fs')
const randomWords = require('random-words')
const YAML = require('js-yaml')
const { assign, values } = Object

const Repository = require('../')
const { tmp, cleanup } = require('./util')

const fixtures = fs.readFileSync(path.join(__dirname, './fixtures.yml'))

function createFactory (stubs = {}) {
  return function Factory (params = {}) {
    return assign({ attributes: params }, stubs)
  }
}

describe('repository: basic', (context) => {
  context('load()', async function (assert, next) {
    let storage = tmp()

    sinon
      .stub(fs, "readFile")
      .yields(null, fixtures)

    const ProxyRepository = proxyquire('../', { fs })
    const store = ProxyRepository(storage, createFactory())

    await store.load()

    assert.ok(fs.readFile.calledOnce, 'storage file read')
    assert.same(values(store.collection).length, 2, 'caches records')

    fs.readFile.restore()
    cleanup(storage, next)
  })

  context('save()', function (assert, next) {
    let storage = tmp(),
      ready = sinon.stub().callsArg(0)

    sinon
      .stub(fs, "writeFile")
      .yields(null, fixtures)

    const ProxyRepository = proxyquire('../', { fs })
    const store = ProxyRepository(null, createFactory({ ready }))
    store.isReady = true
    store.inMemory = false
    store._seed(YAML.safeLoad(fixtures))

    store.save((err) => {
      assert.ok(fs.writeFile.calledOnce, 'saves the repository')

      fs.writeFile.restore()
      cleanup(storage, next)
    })
  })

  context('ready()', async function (assert, next) {
    let storage = tmp(),
      ready = sinon.stub().callsArg(0)

    sinon
      .stub(fs, "exists")
      .yields(true)

    sinon
      .stub(fs, "readFile")
      .yields(null, fixtures)

    const ProxyRepository = proxyquire('../', { fs })
    const store = ProxyRepository(storage, createFactory({ ready }))

    await store.ready()

    assert.ok(ready.calledOnce, 'record made ready')

    fs.exists.restore()
    fs.readFile.restore()
    cleanup(storage, next)
  })

  context('all()', async function (assert, next) {
    let storage = tmp(),
      ready = sinon.stub().callsArg(0)

    const store = Repository(storage, createFactory({ ready }))
    store.isReady = true // skips fs operations

    var response = await store.all()

    assert.ok(Array.isArray(response), 'returns an array')
    assert.same(response.length, 0, 'returns the correct number')

    store._seed(YAML.safeLoad(fixtures))

    var response = await store.all()
    assert.ok(Array.isArray(response), 'returns an array')
    assert.same(response.length, 1, 'returns the correct number')

    cleanup(storage, next)
  })

  context('where({ name, address })', async function (assert, next) {
    let storage = tmp(),
      ready = sinon.stub().callsArg(0),
      entries = YAML.safeLoad(fixtures)

    const store = Repository(storage, createFactory({ ready }))
    store._seed(entries)

    let entry = values(entries)[0]
    let response = await store.where(entry)
    assert.ok(Array.isArray(response), 'returns an array')
    let record = response[0]

    assert.ok(record, 'finds an entry')
    assert.same(record && record.attributes, entry, 'finds the correct entry')

    cleanup(storage, next)
  })

  context('where({ name, address: null })', async function (assert, next) {
    let storage = tmp(),
      ready = sinon.stub().callsArg(0),
      entries = YAML.safeLoad(fixtures)

    const store = Repository(storage, createFactory({ ready }))
    store._seed(entries)

    let entry = values(entries)[0]
    entry.address = null

    try {
      await store.where(entry)
    } catch (err) {
      assert.ok(err, 'throws an error')
      assert.ok(err.notFound, 'not found error')
      assert.ok(err.message, 'entry not found')
      cleanup(storage, next)
    }
  })

  context('where({ name, address: "wrong-address" })', async function (assert, next) {
    let storage = tmp(),
      ready = sinon.stub().callsArg(0),
      entries = YAML.safeLoad(fixtures)

    const store = Repository(storage, createFactory({ ready }))
    store._seed(entries)

    let entry = values(entries)[0]
    let clone = assign({}, entry)
    clone.address = "wrong address"

    try {
      let results = await store.where(clone)
    } catch (err) {
      assert.ok(err, 'throws an error')
      assert.ok(err.notFound, 'not found error')
      assert.ok(err.message, 'entry not found')
      cleanup(storage, next)
    }
  })

  context('where({ name })', async function (assert, next) {
    let storage = tmp(),
      ready = sinon.stub().callsArg(0),
      entries = YAML.safeLoad(fixtures)

    const store = Repository(storage, createFactory({ ready }))
    store._seed(entries)

    let entry = values(entries)[0]
    let clone = assign({}, entry)
    delete clone.address

    let response = await store.where(clone)
    assert.ok(Array.isArray(response), 'returns an array')
    let record = response[0]

    assert.ok(record, 'finds an entry')
    assert.same(record && record.attributes, entry, 'finds the correct entry')
    cleanup(storage, next)
  })

  context('findBy()', async function (assert, next) {
    let storage = tmp(),
      ready = sinon.stub().callsArg(0),
      entries = YAML.safeLoad(fixtures)

    const store = Repository(storage, createFactory({ ready }))
    store._seed(entries)

    let entry = values(entries)[0]

    var record = await store.findBy(entry)
    assert.ok(record, 'finds an entry')
    assert.same(record && record.attributes, entry, 'finds the correct entry')
    cleanup(storage, next)
  })

  context('create()', async function (assert, next) {
    let pending = 2

    let storage = tmp(),
      ready = sinon.stub().callsArg(0),
      entries = YAML.safeLoad(fixtures)

    let params = values(entries)[0]

    const store = Repository(storage, createFactory({ ready }))

    store.on('entry', (entry) => {
      assert.same(entry.attributes, params, 'emits the entry')
      done()
    })

    var entry = await store.create(params)
    assert.same(entry && entry.attributes, params, 'creates an entry')
    done()

    function done () {
      if (--pending) return cleanup(storage, next)
    }
  })

  context('remove()', async function (assert, next) {
    let storage = tmp(),
      ready = sinon.stub().callsArg(0),
      entries = YAML.safeLoad(fixtures)

    let params = values(entries)[0]

    const store = Repository(storage, createFactory({ ready }))

    var entry = await store.create(params)
    assert.same(entry && entry.attributes, params, 'creates an entry')

    await store.remove(params)

    try {
      await store.where(params)
    } catch (err) {
      assert.ok(err, 'throws an error')
      assert.ok(err.notFound, 'not found error')
      assert.ok(err.message, 'entry not found')
      cleanup(storage, next)
    }
  })
})
